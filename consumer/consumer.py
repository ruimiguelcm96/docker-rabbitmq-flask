#!/usr/bin/env python3

import pika
import pymongo
import json

credentials = pika.PlainCredentials( username= "user", password = "password")
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host="rabbit", credentials=credentials)
)

channel = connection.channel()
channel.queue_declare(
    queue="queue"
)

def callback(ch, method, properties, body):
    ## connect to the database mongodb
    client = pymongo.MongoClient("mongodb")
    ## put the data into the collection
    db = client["db"]
    collection = db["rabbit"]
    
    print("Received %r" % body )
    x = collection.insert_one(
        json.loads( body ) ##decode the json
    )
    print( "inserted ", x.inserted_id ) 
    


channel.basic_consume(
    queue="queue",
    on_message_callback=callback,
    auto_ack=True
)

print("Waiting for messages")
channel.start_consuming()
